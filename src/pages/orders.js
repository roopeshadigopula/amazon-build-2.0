import React from 'react'
import Header from '../components/Header'
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/router';

function Orders() {
  const { data: session, status } = useSession();
  const router = useRouter();

  return (
    <div className='bg-gray-100 h-screen'>
      <Header />

      <main className='max-w-screen-lg mx-auto'>
        <div className='flex flex-col p-10 bg-white'>
          <div className='flex items-center space-x-2 mb-5'>
            <h1 className='text-3xl'>Your Orders</h1>
          </div>
          <p>
            {(status === 'authenticated')
              ? (
                <ul>
                  <li>⚒️ ....Construction in progress.... ⚒️</li>
                  <br />
                  <br />
                  <li>Need to set up a back up storage ( such as Firestore db ) from where we can pull the session data.</li>
                  <li>Idea here is to implement a webhook which is invoked on click of Proceed to checkout.</li>
                  <li>This will get triggered and stores the session details in firestore.</li>
                  <li>We can add a check here if the stripe session is completed successfully</li>
                  <li>At this page, we will pull the latest successful session from Firestore db to display the order items.</li>
                </ul>)
              : (<h2>Please sign in to see your orders.</h2>)}
          </p>
          <button
            className='button mt-8'
            onClick={() => router.push('/')}
          >
            Go to Homepage
          </button>
        </div>
      </main>
    </div>
  )
}

export default Orders